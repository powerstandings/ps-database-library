package psdb

import (
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
	"github.com/golang/glog"
)

// Global connection pool pointers
var dbpools = make(map[string]*sql.DB)

// GetConnection returns a connection matching the provided name from the existing
// pools map.
func GetConnection(pool string) *sql.DB {
	return dbpools[pool]
}

// InitNewPool creates a new database pool, adds it to the pools map, and returns
// a connection for use.
func InitNewPool(
	pool_name string,
	hosts []string,
	max_idle int,
	max_open int,
) (*sql.DB, error) {
	num_tries := len(hosts)
	var err error
	var new_db *sql.DB
	for i := 0; i < num_tries; i++ {
		glog.Infof("Trying connection to database at %s\n", hosts[i])
		new_db, err = sql.Open("mysql", hosts[i])
		if err != nil {
			if i == num_tries {
				return new_db, err
			} else {
				glog.Errorf("Unable to connect to database host: %s, trying next host in list %s\n", hosts[i], err)
			}
		} else {
			break
		}
	}
	new_db.SetMaxIdleConns(max_idle)
	new_db.SetMaxOpenConns(max_open)
	dbpools[pool_name] = new_db
	glog.Infof("Initialized new SQL database pool named %s\n", pool_name)

	return new_db, nil
}
