PS Database Pool
----------------

A library that sets up and maintains connection pools to each of the MySQL databases
that Powerstandings currently writes to or reads from.

## Installation

`go get bitbucket.org/noisewaterphd/ps-database-library`

## Usage

Install PS-Database-Pool into your environment and then import it as follows:

`import bitbucket.org/noisewaterphd/ps-database-library`

## Development

If you are working on the library itself, it is easiest to work on it in the 'Go-Gettable' path.

`$GOPATH/src/bitbucket.org/noisewaterphd/ps-database-library`

`go get` simply uses Git to clone dependencies as Git repositories. So, if you have already
used `go get` to install the library, you already have the repo cloned. You can use it as any other
Git repository.

If you have not installed the library using `go get`, you can also simply clone the repository
into its full source path.